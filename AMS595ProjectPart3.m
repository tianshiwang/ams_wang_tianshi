%create function for whole part 3
function finalvalue = AMS595ProjectPart3() 
% variables
numin = 0;     %number of points inside the circle
totalnum = 1;  %total number of points
outcome = [];  %array to store calculated-pi values
error = 1;


%(a) guest input
prompt = 'What significant figure do you choose?';
inputvalu = input(prompt);      %input value
signum = 10^(1-(inputvalu+1));  %significant number 

%(b) plot squre and circle
%draw square
squarex = [0 1 1 0 0];
squarey = [0 0 1 1 0];
plot(squarex, squarey);
hold on
%draw 1/4 circle
circlex = 0;
circley = 0;
r = 1;
th = linspace(pi/2,0,100);
circlea = r*cos(th) + circlex;
circleb = r*sin(th) + circley;
plot(circlea,circleb);
hold off

hold on
% while loop to calculate PI with input significant figures
  while error >= signum
    
    %generate random value
    x = rand;
    y = rand;
    
    %calculate pi value
    if x^2 + y^2 <= r^2
        numin = numin + 1;
        plot(x,y,'ro');   % plot points inside the circle
    else 
        plot(x,y,'go');   % plot points outside the circle
    end
    
    outcome(totalnum) = (numin / totalnum) *4;   %store different pi value
  
    % find the interval of last 30 outcomes
    if totalnum >50
        error = max(outcome(totalnum -30 :totalnum))-min(outcome(totalnum -30 :totalnum));
    else
        error = 1;
    end
    
    % step counter also total number of points 
    totalnum = totalnum + 1;
  end
hold off

%total steps
step = totalnum -1;
 

%(c)display outcome
if inputvalu == 1
    fprintf('%.0f\n',outcome(step));
else
    digits(inputvalu);
    finanalansw = double(vpa(outcome(step)));
    disp(finanalansw);
end   



%print on the plot
if inputvalu == 1
    answerplot = num2str(outcome(step),0);
    text(0.5,0.5,answerplot,'FontSize',25);
else    
    answerplot = num2str(finanalansw);
    text(0.5,0.5,answerplot,'FontSize',25);
end    

if inputvalu == 1
    finalvalue = num2str(outcome(step),0);
   
else    
    finalvalue = num2str(finanalansw);
    
end    

end


