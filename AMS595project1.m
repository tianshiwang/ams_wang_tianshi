outcome = []; %Array of calculated pi value
time = []; % time of running function calpi
totalnumA=[]; % Array of various number of total points
difference= [];% difference between calculate pi and true pi 
digits(9);
truepi = double(vpa(pi));
%counter 
i = 1;

% increasing the total number of points
for totalnum = 1000:50:10000 
    
    totalnumA(i) = totalnum;
    outcome(i) = calpi(totalnum);   
    func =@() calpi(totalnum);
    time(i) = timeit(func);
    difference(i) = abs(truepi -outcome(i));
    i = i+1;
end

%figure 1, plot the value of pi and the difference from the true value of pi
plot(totalnumA,outcome,'r');
hold on
plot(totalnumA,difference,'g');
plot([1 totalnum],[1 1]*pi, 'LineWidth', 1);
hold off

%figure 2, plot the precision vs computational cost
figure
plot(totalnumA, time,'r');

%pi function called calpi
function result = calpi(totalnum)
 
numin = 0;% number of points inside the circle
 


%body of the function 
for i = 1:totalnum
    x=rand;
    y=rand;
    if x^2+y^2<=1
        numin =numin +1;
    end
end  
result = numin/totalnum*4;

end

